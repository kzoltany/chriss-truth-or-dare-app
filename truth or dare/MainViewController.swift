//
//  MainViewController.swift
//  truth or dare
//
//  Created by Charles Benedict on 23/02/2015.
//  Copyright (c) 2015 teamrune. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
  
  // Member Variables
  
  var truth: Bool = false
  var dare: Bool = false
  
  // Outlets && Actions
  
  @IBAction func truthButton(sender: AnyObject) {
    truth = true
    dare = false
    truthOrDareLabel.text = "Truth: \(truthDareBook.randomTruth())"
    view.backgroundColor = UIColor(red: 48/255, green: 121/255, blue: 171/255, alpha: 1)
  }
  
  @IBAction func dareButton(sender: AnyObject) {
    dare = true
    truth = false
    truthOrDareLabel.text = "Dare: \(truthDareBook.randomDare())"
    view.backgroundColor = UIColor(red: 99/255, green: 122/255, blue: 145/255, alpha: 1)
  }
  
  @IBOutlet weak var truthOrDareLabel: UILabel!
  
  // References
  
  let truthDareBook = DareBook()
  
  // MARK: - View Stuff

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    truthOrDareLabel.text = "Welcome to truth or dare (Adult Adition)"
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - Status Bar Style
  
  override func preferredStatusBarStyle() -> UIStatusBarStyle {
    return UIStatusBarStyle.LightContent
  }


}

