//
//  DareBooks.swift
//  truth or dare
//
//  Created by Charles Benedict on 23/02/2015.
//  Copyright (c) 2015 teamrune. All rights reserved.
//

import Foundation

struct DareBook {
  
  let dares = [
    "do the dishes",
    "hump someones head for 10 seconds",
    "grind on a chair",
    "lick your foot",
    "Seven Minutes in heaven with someone",
    "Fuck a teddy bear"
  ]
  
  func randomDare() -> String {
    var unsignedArrayCount = UInt32(dares.count)
    var unsignedRandomNumber = arc4random_uniform(unsignedArrayCount)
    var randomNumber = Int(unsignedRandomNumber)
    
    return dares[randomNumber]
  }
  
  let truth = [
    "Have you ever masticated on your phone?",
    "Who was your most recent girlfriend?",
    "Who was your “Wet dream” about?",
    "When was the last time you “Did it”?",
    "Who do you find attractive",
    "Do you have a crush? If so who is it?"
  ]
  
  func randomTruth() -> String {
    var unsignedArrayCount = UInt32(truth.count)
    var unsignedRandomNumber = arc4random_uniform(unsignedArrayCount)
    var randomNumber = Int(unsignedRandomNumber)
    
    return truth[randomNumber]
  }
}